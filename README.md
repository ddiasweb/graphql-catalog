# My graphql API

My graphql API is a graphql API with AWS Appsync.

## Developing

Use make dev to run local.

```bash
make dev
```

## Testing

Use make test to lint and test.

```bash
make test
```

## Build

Build doc and app

```bash
make build
```

## Deploy

Deploy

```bash
make deploy
```

## License

[MIT](https://choosealicense.com/licenses/mit/)