import 'should'
import { request, response } from '../src/pipelineFunctions/getArea.js'

describe('getArea', function () {
  describe('request', function () {
    it('filter should be eql {}', function () {
      const ctx = {
        arguments: {}
      }
      const expectedResult = {
        operation: 'Invoke',
        payload: { collectionName: 'areas', filter: {} }
      }
      const result = request(ctx)
      result.should.be.eql(expectedResult)
    })

    it('filter should be eql { id: 1 }', function () {
      const ctx = {
        arguments: { id: 1 }
      }
      const expectedResult = {
        operation: 'Invoke',
        payload: { collectionName: 'areas', filter: { id: 1 } }
      }
      const result = request(ctx)
      result.should.be.eql(expectedResult)
    })
  })

  describe('response', function () {
    it('response should be eql {}', function () {
      const ctx = {
        result: [{}]
      }
      const expectedResult = {}
      const result = response(ctx)
      result.should.be.eql(expectedResult)
    })

    it('response should be eql { id: 1 }', function () {
      const ctx = {
        result: [{ id: 1 }]
      }
      const expectedResult = { id: 1 }
      const result = response(ctx)
      result.should.be.eql(expectedResult)
    })
  })
})
