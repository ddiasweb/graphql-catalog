import util from 'util';

const getTexts = (args) => args.map(arg => typeof arg === 'object' ? util.inspect(arg, false, null, true) : arg)

export function log(...args) {
    console.log('LOG:', ...getTexts(args))
}

export function info(...args) {
    console.info('INFO:', ...getTexts(args))
}

export function warn(...args) {
    console.warn('WARN:', ...getTexts(args))
}

export function error(...args) {
    console.error('ERROR:', ...getTexts(args))
}