import { type Collection as MongoCollection, MongoClient } from 'mongodb'

const mongoUrl = process.env.MONGO_URL;
if (!mongoUrl) throw new Error('Variable MONGO_URL is not defined.')
const client = new MongoClient(mongoUrl);

const mongoDbName = process.env.MONGO_DB_NAME;
if (!mongoDbName) throw new Error('Variable MONGO_DB_NAME is not defined.')

export class Collection {
  collectionName: string;
  collection: MongoCollection;
  constructor(collectionName: string) {
    this.collectionName = collectionName
  }

  async connect() {
    await client.connect()
    const db = client.db(mongoDbName)
    this.collection = db.collection(this.collectionName)
  }

  async get(filter: object) {
    await this.connect();
    return await this.collection.find(filter).toArray()
  }

  async query(query: object[]) {
    await this.connect();
    return await this.collection.aggregate(query).toArray()
  }

  async put(item: object) {
    await this.connect();
    return await this.collection.insertOne(item)
  }
}
