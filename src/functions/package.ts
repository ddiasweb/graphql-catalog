import { Collection } from '../adapters/mongo'
import { info } from '../helpers/log.js'

export async function Query (payload: { state: string; city: string; skus: [string] }) {
  info('payload:', payload)
  const { state, city, skus } = payload
  const filterAreas = { $or: [{ states: state }, { cities: city } ] }
  const areas = await new Collection('areas').get(filterAreas)
  const condAreasIds = areas.map((area) => ({
    $in: [
      area.id,
      '$$when.areas.includes'
    ]
  }))
  const queryProducts = [
    {
      $match: { sku: { $in: skus } }
    },
    {
      $addFields: {
        when: {
          $filter: {
            input: '$when',
            as: 'when',
            cond: {
              $or: [
                {
                  $not: '$$when.areas'
                },
                ...condAreasIds
              ]
            }
          }
        }
      }
    }
  ]
  info('queryProducts:', queryProducts)
  const promiseProducts = new Collection('products').query(queryProducts)
  const [products] = await Promise.all([promiseProducts])
  info('products:', products)
  return {
    areas,
    products
  }
}
