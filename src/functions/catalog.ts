import { Collection } from '../adapters/mongo'
import { info } from '../helpers/log.js'

export async function Query (payload: { collectionName: string; pipeline: [object] }) {
  info('payload:', payload)
  const { collectionName, pipeline } = payload
  if (!collectionName) {
    throw new Error('collectionName undefined.')
  }
  const collection = new Collection(collectionName)
  const response =  await collection.query(pipeline)
  info('response:', response)
  return response;
}
