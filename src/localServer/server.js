import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';
import { readFileSync } from 'fs'

import * as getArea from '../pipelineFunctions/getArea.js'
import * as getAreas from '../pipelineFunctions/getAreas.js'
import * as getProduct from '../pipelineFunctions/getProduct.js'
import * as getProducts from '../pipelineFunctions/getProducts.js'
import * as getTechnology from '../pipelineFunctions/getTechnology.js'
import * as getTechnologies from '../pipelineFunctions/getTechnologies.js'
import * as getPaymentType from '../pipelineFunctions/getPaymentType.js'
import * as getPaymentTypes from '../pipelineFunctions/getPaymentTypes.js'
import * as getViability from '../pipelineFunctions/getViability.js'
import * as getViabilities from '../pipelineFunctions/getViabilities.js'
import * as getPackage from '../pipelineFunctions/getPackage.js'
import { Query as catalogDataSource } from '../../build/localServer/catalog.js'
import { Query as packageDataSource } from '../../build/localServer/package.js'

const typeDefs = readFileSync('src/schema.graphql').toString('utf-8')

const resolver = ({ request, response }, Query) => async (a, args) => {
    const ctx = {
        arguments: args
    }
    return response({ result: await Query(request(ctx).payload) })
}

const resolvers = {
  Query: {
    area: resolver(getArea, catalogDataSource),
    areas: resolver(getAreas,  catalogDataSource),
    product: resolver(getProduct, catalogDataSource),
    products: resolver(getProducts, catalogDataSource),
    technology: resolver(getTechnology, catalogDataSource),
    technologies: resolver(getTechnologies, catalogDataSource),
    paymentType: resolver(getPaymentType, catalogDataSource),
    paymentTypes: resolver(getPaymentTypes, catalogDataSource),
    viability: resolver(getViability, catalogDataSource),
    viabilities: resolver(getViabilities, catalogDataSource),
    package: resolver(getPackage, packageDataSource),
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

const { url } = await startStandaloneServer(server);
console.log(`🚀 Server ready at ${url}`);