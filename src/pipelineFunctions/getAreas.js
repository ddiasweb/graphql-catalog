export function request (ctx) {
  console.log('request ctx:', ctx)
  const { states, cities } = ctx.arguments
  const filter = { $or: [{ states: { $in: states } }, { cities: { $in: cities } }] }
  return {
    operation: 'Invoke',
    payload: { collectionName: 'areas', filter }
  }
}

export function response (ctx) {
  console.log('response ctx:', ctx)
  return ctx.result
}
