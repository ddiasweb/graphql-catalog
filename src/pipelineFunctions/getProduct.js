export function request (ctx) {
  console.log('request ctx:', ctx)
  const { sku, anyAreas } = ctx.arguments
  const pipeline = []
  if (sku) {
    pipeline.push({
      $match: { sku }
    })
  }
  if (anyAreas) {
    const condAreasIds = anyAreas.map((area) => ({
      $in: [ area, '$$when.areas.includes' ]
    }))
    pipeline.push({
      $match: { $or: [
        { areas: { $exists: false } },
        { 'areas.includes': { $in: anyAreas } }]
      }
    },{
      $addFields: {
        'combinationRules.when': {
          $filter: {
            input: '$combinationRules.when',
            as: 'when',
            cond: { $or: [{ $not: '$$when.areas' }, ...condAreasIds ] }
          }
        }
      }
    })
  }  
  return {
    operation: 'Invoke',
    payload: { 
      collectionName: 'products', 
      pipeline
    }
  }
}

export function response (ctx) {
  console.log('response ctx:', ctx)
  return ctx.result[0]
}
