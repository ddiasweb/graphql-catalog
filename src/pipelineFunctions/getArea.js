export function request (ctx) {
  console.log('request ctx:', ctx)
  const filter = ctx.arguments
  return {
    operation: 'Invoke',
    payload: { collectionName: 'areas', filter }
  }
}

export function response (ctx) {
  console.log('response ctx:', ctx)
  return ctx.result[0]
}
