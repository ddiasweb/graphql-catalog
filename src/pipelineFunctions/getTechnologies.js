export function request (ctx) {
  console.log('request ctx:', ctx)
  const { ids } = ctx.arguments
  const filter = { id: { $in: ids } }
  return {
    operation: 'Invoke',
    payload: { collectionName: 'technologies', filter }
  }
}

export function response (ctx) {
  console.log('response ctx:', ctx)
  return ctx.result
}
