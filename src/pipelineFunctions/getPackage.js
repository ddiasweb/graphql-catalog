export function request (ctx) {
  console.log('request ctx:', ctx)
  const { states, cities, skus } = ctx.arguments
  return {
    operation: 'Invoke',
    payload: { states, cities, skus }
  }
}

export function response (ctx) {
  console.log('response ctx:', ctx)
  return ctx.result
}
