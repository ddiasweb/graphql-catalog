import { MongoClient } from 'mongodb'
import { readFile } from 'fs/promises'

const mongoUrl = process.env.MONGO_URL;
if (!mongoUrl) throw new Error('Variable MONGO_URL is not defined.')
const client = new MongoClient(mongoUrl);

const mongoDbName = process.env.MONGO_DB_NAME;
if (!mongoDbName) throw new Error('Variable MONGO_DB_NAME is not defined.')

const loadData = async () => {

    console.info('Connecting...')
    await client.connect()
    console.info('Conected!')

    const db = client.db(mongoDbName)

    const collections = [
        [ 'areas', 'id' ],
        [ 'products', 'sku' ],
        [ 'technologies', 'id' ],
        [ 'paymentTypes', 'id' ],
        [ 'viabilities', 'id' ]
    ];

    for( const [ collectionName, key ] of collections ) {
        console.log('Inserting into:', collectionName)
        const mongoCollection = db.collection(collectionName)
        const items = JSON.parse(
            await readFile(
                new URL(`../data/${mongoCollection.collectionName}.json`, import.meta.url)
            )
        );
        await mongoCollection.deleteMany({ $or: items.map(item => ({ [key]: item[key] })) })
        await mongoCollection.createIndex({ [key]: 1 }, { unique: true })
        await mongoCollection.insertMany(items)
    }

    console.info('Done!')
    process.exit();
}

loadData();