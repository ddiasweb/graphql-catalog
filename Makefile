#!make
include .env
PATH:=node_modules/.bin:${PATH}
export

help: ## This help
	@echo The following are some of the valid targets for this Makefile:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sed -n 's/^.*:\(.*\): .*\## \(.*\)/  \1 - \2/p'

.env:
	cp .env.example .env

dev: node_modules test clean build/localServer ## Run local
	node src/localServer/server.js

build/localServer: src/functions/*
	esbuild --format=esm --external:mongodb --bundle $^ --outdir=build/localServer --watch=forever &
	sleep 2

node_modules: install

install:
	npm install --ignore-scripts

update:
	npx npm-check-updates --doctor -u --doctorInstall 'make install' --doctorTest 'make test' --removeRange

lint:
	eslint src test

lint-fix:
	eslint src test --fix

test: lint ## Lint and test
	mocha

test-debug:
	LOG_LEVEL=DEBUG mocha

load-data:
	node tools/loadData.js

build/docs:
	jsdoc -c jsdoc.json

build/serverless:
	serverless package --package build/serverless

build: build/serverless build/docs ## Build docs and app

clean:
	rm -rf build

deploy: build/serverless build/docs ## Deploy
	serverless deploy --package build/serverless

.PHONY: help dev install update lint test clean deploy
