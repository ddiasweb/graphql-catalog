module.exports = {
    "env": {
        "es2021": true,
        "node": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:sonarjs/recommended",
        "plugin:mocha/recommended"
    ],
    "overrides": [
        {
            "files": ["*.graphql"],
            "extends": "plugin:@graphql-eslint/schema-recommended",
            "parserOptions": {
               "schema": "src/schema.graphql"
            },
            "rules": {
                "@graphql-eslint/strict-id-in-types": "off",
                "@graphql-eslint/naming-convention": "off"
            }
        }
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "@typescript-eslint",
        "mocha",
        "sonarjs"
    ],
    "rules": {
    }
}
